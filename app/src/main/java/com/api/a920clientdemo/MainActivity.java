package com.api.a920clientdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etAmount, etTip,etStatus,etResult,etStatusRefund, etResultRefund, etRefundAmount, etRefundOrderNo;
    private Switch sPrintReceipt, sPrintRefund;
    private static final String IOT_PACKAGE = "com.iot";
    private static final String IOT_ACTIVITY = "com.iot.login.LoginActivity";
    private static final String IOT_TYPE = "iotapp";
    private static final String IOT_STATUS = "iot_transaction_status";
    private static final String IOT_RESULT = "iot_transaction_payload";
    private static final String IOT_PARAM = "param";
    private static final String IOT_NEEDPRINT = "needprint";
    private static final String IOT_IOTTYPE = "iottype";
    private static final String IOT_AMOUNT = "amount";
    private static final String IOT_TIP = "tip";
    private static final String IOT_TYPE_PAY = "pay";
    private static final String IOT_TYPE_REFUND = "refund";
    private static final String IOT_ORDER_NO = "iot_order_no";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etAmount = findViewById(R.id.etAmount);
        etTip = findViewById(R.id.etTip);
        etStatus = findViewById(R.id.etStatus);
        etResult = findViewById(R.id.etResult);
        sPrintReceipt = findViewById(R.id.sPrintReceipt);
        sPrintRefund = findViewById(R.id.sPrintRefund);
        etStatusRefund = findViewById(R.id.etStatusRefund);
        etResultRefund = findViewById(R.id.etResultRefund);
        etRefundAmount = findViewById(R.id.etRefundAmount);
        etRefundOrderNo = findViewById(R.id.etRefundOrderNo);
    }

    public void btPay(View v){
        etStatus.setText("");
        etResult.setText("");
        String sAmount = etAmount.getText()+"";
        if(sAmount.isEmpty()){
            return;
        }
        Boolean needPrint = sPrintReceipt.isChecked();

        String sTip = etTip.getText()+"";
        Double dAmount = Double.valueOf(sAmount);
        Double dTip = null;
        if(sTip.length() > 0){
            dTip = Double.valueOf(sTip);
        }

        ComponentName componentName = new ComponentName(IOT_PACKAGE, IOT_ACTIVITY);
        Intent intent = new Intent();
        intent.setComponent(componentName);
        Bundle bundle = new Bundle();
        bundle.putBoolean(IOT_TYPE, true);
        bundle.putBoolean(IOT_NEEDPRINT, needPrint);
        bundle.putString(IOT_IOTTYPE, IOT_TYPE_PAY);
        bundle.putDouble(IOT_AMOUNT,dAmount);
        if(dTip != null){
            bundle.putDouble(IOT_TIP, dTip);
        }

        intent.putExtra(IOT_PARAM,bundle);
        try{
            startActivityForResult(intent, 1);
        }catch (Exception e){
            Toast.makeText(this, "Failed, please make sure IOTPay APP is installed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


        //tvResult.setText("amount:" + sAmount+", tip: " + sTip+", needPrint:" + needPrint);
    }
    public void refund(View v){
        etStatusRefund.setText("");
        etResultRefund.setText("");

        Boolean needPrint = sPrintRefund.isChecked();
        String sOrderNo = etRefundOrderNo.getText() +"";
        String sRefundAmount = etRefundAmount.getText()+"";
        Double dRefundAmount = 0.0;
        if(!sRefundAmount.isEmpty()){
            dRefundAmount = Double.valueOf(sRefundAmount);
        }


        ComponentName componentName = new ComponentName(IOT_PACKAGE, IOT_ACTIVITY);
        Intent intent = new Intent();

        intent.setComponent(componentName);
        Bundle bundle = new Bundle();
        bundle.putBoolean(IOT_TYPE, true);

        bundle.putString(IOT_IOTTYPE, IOT_TYPE_REFUND);
        bundle.putBoolean(IOT_NEEDPRINT, needPrint);
        bundle.putString(IOT_ORDER_NO, sOrderNo);
        bundle.putDouble(IOT_AMOUNT, dRefundAmount);
        intent.putExtra(IOT_PARAM,bundle);
        try{
            startActivityForResult(intent, 1);
        }catch (Exception e){
            Toast.makeText(this, "Failed, please make sure IOTPay APP is installed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!=null){
            Bundle bundle = data.getBundleExtra(IOT_PARAM);
            String iotType = bundle.getString(IOT_IOTTYPE);
            if(iotType.equals(IOT_TYPE_REFUND)){
                etStatusRefund.setText(data.getStringExtra(IOT_STATUS));
                etResultRefund.setText(data.getStringExtra(IOT_RESULT));

            }else{
                etStatus.setText(data.getStringExtra(IOT_STATUS));
                etResult.setText(data.getStringExtra(IOT_RESULT));

            }
        }
    }


}
